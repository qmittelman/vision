# -*- coding: utf-8 -*-
"""

@author: quentinmittelman

Implementation of the Canny edge detector. 
"""

import numpy as np 

import matplotlib.pyplot as plt 

import scipy.ndimage as ndimage

plt.rcParams['image.cmap'] = 'gray' 


def load_image(name,crop_window=-1): 
    I0=plt.imread(name)
    W, H = I0.shape[0:2]
    if len(I0.shape) == 3:
        I = np.zeros(I0.shape[0:2], "uint8")
        for i in range(W):
            for j in range(H):
                I[i][j] = int(0.2989*I0[i][j][0] + 0.5870*I0[i][j][1] +0.1140*I0[i][j][2])
    else:
        I = I0
    if crop_window!=-1:
        I=I[crop_window[0]:crop_window[1],crop_window[2]:crop_window[3]]
    I=I.astype('float')/255 
    return I


def compute_gradient(I, sigma=0):
    Ib=ndimage.gaussian_filter(I, sigma)
    hf = np.array([[-1,1]])
    vf = np.array([[1], [-1]])
    Ih = ndimage.convolve(Ib, hf)
    Iv = ndimage.convolve(Ib, vf)
    Ig = np.sqrt(Ih*Ih + Iv*Iv)
    return (Ih, Iv, Ig)

def grad_tresh(I, t, sigma):
    Ih, Iv, Ig = compute_gradient(I, sigma)
    def tresh(x,t):
        if x >= t:
            return 1
        else: 
            return 0
    tresh_v = np.vectorize(tresh)
    return tresh_v(Ig, t)


def nms(gradient):
    dw = (1,0)
    dnw = (np.sqrt(2)/2, np.sqrt(2)/2)
    dn =(0,1)
    dne = (-np.sqrt(2)/2, np.sqrt(2)/2)
    l1, l2 = gradient[0].shape
    g1 =np.zeros((l1+2, l2+2))
    g2 = np.zeros((l1+2, l2+2))
    for i in range(l1):
        for j in range(l2):
            g1[i+1][j+1], g2[i+1][j+1] = gradient[0][i][j], gradient[1][i][j]
    def p(x):
        a, b, c = x
        return a
    res = np.zeros((l1, l2))
    for i in range(l1):
        for j in range(l2):
            d_w = (abs(np.dot(dw, (g1[i+1][j+1],g2[i+1][j+1]))) , dw, (1,0))              #Checking the direction orthogonal to the edge
            d_nw = (abs(np.dot(dnw, (g1[i+1][j+1],g2[i+1][j+1]))) , dnw, (1,1))
            d_n = (abs(np.dot(dn, (g1[i+1][j+1],g2[i+1][j+1]))) , dn, (0,1))
            d_ne = (abs(np.dot(dne, (g1[i+1][j+1],g2[i+1][j+1]))) , dne, (-1,1))
            l = [d_w, d_n, d_ne, d_nw]
            m = max(l, key=p)
            if np.dot(m[1], (g1[i+1][j+1],g2[i+1][j+1])) <0:            #Checking that the pixel is a maximum in this direction
                if np.dot(m[1], (g1[i+1-m[2][0]][j+1-m[2][1]],g2[i+1-m[2][0]][j+1-m[2][1]]))>0:  mistake I guess?
                    res[i][j] = 1
    return res

            
def extract(I, t, sigma):
    I1 = grad_tresh(I, t, sigma)
    g = np.gradient(I1)
    return nms(g)



def canny_edges(I, t1, t2, sigma): #t1 < t2
    I1 = grad_tresh(I, t1, sigma)
    I2 = grad_tresh(I, t2, sigma)
    g1 = np.gradient(I1)
    g2 = np.gradient(I2)
    nms1 = nms(g1)
    nms2 = nms(g2)
    l1, l2 = nms1.shape
    to_check_n = []
    already_checked = np.zeros((l1,l2))
    res = np.copy(nms2)
    for i in range(l1):
        for j in range(l2):
            if nms2[i][j]:
                to_check_n.append((i,j))
    while len(to_check_n)>0:
        (i,j) = to_check_n.pop()
        if not already_checked[i][j]:
            already_checked[i][j] = 1
            if nms1[i][j]:
                res[i][j] = 1
                for a in [-1,0, 1]:
                    if i+a>=0:
                        if i+a < l1:
                            for b in [-1,0, 1]:
                                if abs(a)+abs(b) >0:
                                    if j+b>=0:
                                        if j+b < l2:
                                            to_check_n.append((i+a,j+b))
    return res


