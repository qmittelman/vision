# -*- coding: utf-8 -*-
"""
Created on Tue Oct 15 01:23:19 2019

@author: quentinmittelman

Implementation of the Mean shift segmentation. 
"""

import numpy as np 

import matplotlib.pyplot as plt 

import scipy.ndimage as ndimage

plt.rcParams['image.cmap'] = 'gray' 

from skimage.color import rgb2lab,lab2rgb

def MS_step(data,x,sigma):
    listx = []
    s = x.copy()
    for xi in data:
        a = (np.array(x)/sigma) - (np.array(xi)/sigma)
        b = np.dot(a, a)
        c = -0.5*np.exp(-0.5*b)
        listx.append(c)
        s+=c*np.array(xi)
    if sum(listx) == 0:
        return (0*np.array(x))
    s = s/sum(listx)
    return s

def MS_point(data,x,sigma):
    xx = x.copy()
    y = MS_step(data,xx,sigma)
    while np.dot(np.array(xx)-np.array(y),np.array(xx)-np.array(y)) > 0.01:
        xx = y.copy()
        y = MS_step(data,xx,sigma)
    return y

def MS(data,sigma):
    labels=np.zeros([data.shape[0],1], dtype=int)
    modes=[]
    current_label = 0
    for i, xi in enumerate(data):
        m = np.array(MS_point(data, xi, sigma))
        no_label = True
        for j in range(i):
            if np.sqrt(sum(((np.array(modes[labels[j][0]])-m)/sigma)**2 )) <0.5:
                labels[i] = labels[j]
                no_label = False
        if no_label:
            modes.append(m)
            labels[i] = current_label
            current_label += 1
    return labels,modes

def MS_segmentation(I):
    N, M, x = I.shape
    l = []
    for i in range(N):
        for j in range(M):
            a, b, c = I[i][j]
            l.append(np.array([a, b, c, i, j]))
    l = np.array(l)
    sigma = np.std(l, axis=0)/1.5
    labels, modes = MS(l, sigma)
    R = np.zeros(I.shape)
    for i, xi in enumerate(l):
        a, b, c, x, y = xi
        am, bm, cm, xm, ym = modes[labels[i][0]]
        R[int(x)][int(y)] = [am, bm, cm]
    return lab2rgb(R)
    
